/**
 * Created by Dmitry on 6/7/2015.
 */
import java.text.DecimalFormat;

public class RoomCarpet {

    private RoomDimension size;
    private double carpetCost;

    public RoomCarpet(RoomDimension dim, double cost){
        size = dim;
        carpetCost = cost;
    }

    public double getCarpetCost() {
        return carpetCost * size.getArea();
    }

    public String toString() {
        DecimalFormat currency = new DecimalFormat("#,##0.00");
        String str = size + "  Area: " + size.getArea() + "\nCarpet cost:  $" + currency.format(getCarpetCost());
        return str;
    }
}
